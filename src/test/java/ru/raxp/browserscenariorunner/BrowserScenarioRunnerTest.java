package ru.raxp.browserscenariorunner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.raxp.browserscenariorunner.actions.Command;
import ru.raxp.browserscenariorunner.actions.OpenURLCommand;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class BrowserScenarioRunnerTest {
    private BrowserScenarioRunner runner = new BrowserScenarioRunner(null, null);

    @Before
    public void setUp() {
        ArrayList<Command> commands = new ArrayList<Command>();
        commands.add(new OpenURLCommand("openUrl", "https://netcracker.com/", "Open Netcracker"));
        runner.addCommands(commands);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void run() {
        runner.run();
        assertEquals("Netcracker - Home", runner.getBrowserTitle());
        runner.closeBrowser();
    }
}