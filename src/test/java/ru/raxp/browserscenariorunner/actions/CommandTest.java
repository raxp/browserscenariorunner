package ru.raxp.browserscenariorunner.actions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.raxp.browserscenariorunner.BrowserScenarioRunner;

import java.io.File;

import static org.junit.Assert.*;

public class CommandTest {
    private OpenURLCommand openURLCommand;
    private ClickCommand clickCommand;
    private CheckElementVisibleCommand checkElementVisibleCommand;
    private SetValueCommand setValueCommand;
    private ScreenshotCommand screenshotCommand;
    private WebDriver driver;

    @Before
    public void setUp() throws Exception {
        File currentDir = new File(".");
        openURLCommand = new OpenURLCommand("openUrl", String.format("file://%s/misc/test.html", currentDir.getCanonicalPath()), "Open test html page");
        clickCommand = new ClickCommand("Click", "//*[@id=\"test-button\"]", "Click test button");
        checkElementVisibleCommand = new CheckElementVisibleCommand("checkElementVisible", "//*[@id=\"test-text\"]", "Check text input visible");
        setValueCommand = new SetValueCommand("setValue", "//*[@id=\"test-text\"] | test", "Set value to text input");
        screenshotCommand = new ScreenshotCommand("Screenshot", "", "Take a screenshot");

        System.setProperty("webdriver.chrome.driver", BrowserScenarioRunner.DEFAULT_CHROME_DRIVER_PATH);
        driver = new ChromeDriver();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void run() {
        openURLCommand.run(driver);
        assertEquals("Test html", driver.getTitle());

        assertFalse(checkElementVisibleCommand.run(driver));

        clickCommand.run(driver);
        assertTrue(checkElementVisibleCommand.run(driver));

        setValueCommand.run(driver);
        assertEquals("test", driver.findElement(By.xpath("//*[@id=\"test-text\"]")).getAttribute("value"));

        screenshotCommand.run(driver);
        File file = new File(String.format("%s/screenshot1.png", ScreenshotCommand.DEFAULT_SCREENSHOT_PATH));
        assertTrue(file.exists());

        driver.close();
    }
}