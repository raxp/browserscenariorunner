package ru.raxp.browserscenariorunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.raxp.browserscenariorunner.actions.Command;
import ru.raxp.browserscenariorunner.actions.ScreenshotCommand;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class BrowserScenarioRunner {
    public static final String DEFAULT_SCENARIO_FILE_PATH = "test.json";
    public static final String DEFAULT_CHROME_DRIVER_PATH = "chromedriver.exe";

    private String chromeDriverPath;
    private ArrayList<Command> commands;
    WebDriver driver;

    public BrowserScenarioRunner(String chromeDriverPath, String screenshotFolderPath) {
        this.chromeDriverPath = chromeDriverPath != null ? chromeDriverPath : DEFAULT_CHROME_DRIVER_PATH;
        ScreenshotCommand.screenshotPath =
                screenshotFolderPath != null ? screenshotFolderPath : ScreenshotCommand.DEFAULT_SCREENSHOT_PATH;

        this.commands = new ArrayList<Command>();

        System.setProperty("webdriver.chrome.driver", this.chromeDriverPath);
        driver = new ChromeDriver();
    }

    //Get all commands from a file scenario
    public boolean populateCommandsFromFile(String scenarioFilePath) {
        ObjectMapper mapper = new ObjectMapper();

        if (scenarioFilePath == null)
            scenarioFilePath = DEFAULT_SCENARIO_FILE_PATH;

        try {
            File file = new File(scenarioFilePath);
            commands = mapper.readValue(file, new TypeReference<ArrayList<Command>>() {});
        } catch (JsonParseException e) {
            System.err.println("Invalid JSON");
            return false;
        } catch (JsonMappingException e) {
            System.err.println("Invalid JSON format. It must contain array of objects, that contains action, params and description fields");
            return false;
        } catch (IOException e) {
            System.err.printf("Could not open file: %s\n", scenarioFilePath);
            return false;
        }

        return true;
    }

    public void addCommands(ArrayList<Command> commands) {
        this.commands.addAll(commands);
    }

    public void cleanCommands() {
        this.commands.clear();
    }

    //Run all commands from commands ArrayList
    public void run() {
        System.out.println("Executing...");

        for (Command command : commands) {
            command.run(driver);
        }
    }

    public String getBrowserTitle() {
        return driver.getTitle();
    }

    public void closeBrowser() {
        driver.close();
    }
}
