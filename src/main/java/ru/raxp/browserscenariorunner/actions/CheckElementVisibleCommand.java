package ru.raxp.browserscenariorunner.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckElementVisibleCommand extends Command {
    public CheckElementVisibleCommand() {
        super();
    }

    public CheckElementVisibleCommand(String action, String params, String description) {
        super(action, params, description);
    }

    public boolean run(WebDriver driver) {
        boolean visible = false;
        try {
            visible = driver.findElement(By.xpath(params)).isDisplayed();
            if (visible) {
                System.out.printf("%s is visible\n", params);
            } else {
                System.out.printf("%s is not visible\n", params);
            }
        } catch (NullPointerException e) {
            System.out.printf("No such element on page: %s\n", params);
        }

        return visible;
    }
}
