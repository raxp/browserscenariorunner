package ru.raxp.browserscenariorunner.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SetValueCommand extends Command {
    public SetValueCommand() {
        super();
    }

    public SetValueCommand(String action, String params, String description) {
        super(action, params, description);
    }

    public boolean run(WebDriver driver) {
        Pattern pattern = Pattern.compile("(.*)\\s+\\|\\s+(.*)");
        Matcher matcher = pattern.matcher(params);

        try {
            matcher.find();
            String xpath = matcher.group(1);
            String text = matcher.group(2);
            System.out.printf("Setting value %s to %s\n", text, xpath);

            Actions actions = new Actions(driver);
            WebElement element = driver.findElement(By.xpath(xpath));
            actions.moveToElement(element);
            actions.click();
            actions.sendKeys(text);
            actions.build().perform();
            return true;
        } catch (Exception e) {
            System.err.println("Could not set value to the element");
            return false;
        }
    }
}
