package ru.raxp.browserscenariorunner.actions;

import org.openqa.selenium.WebDriver;

public class OpenURLCommand extends Command {
    public OpenURLCommand() {
        super();
    }

    public OpenURLCommand(String action, String params, String description) {
        super(action, params, description);
    }

    public boolean run(WebDriver driver) {
        System.out.printf("Opening URL %s\n", params);
        driver.get(params);
        return true;
    }
}
