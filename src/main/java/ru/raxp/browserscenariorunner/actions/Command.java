package ru.raxp.browserscenariorunner.actions;

import com.fasterxml.jackson.annotation.*;
import org.openqa.selenium.WebDriver;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "action", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ClickCommand.class, name = "Click"),
        @JsonSubTypes.Type(value = OpenURLCommand.class, name = "openUrl"),
        @JsonSubTypes.Type(value = SetValueCommand.class, name = "setValue"),
        @JsonSubTypes.Type(value = ScreenshotCommand.class, name = "Screenshot"),
        @JsonSubTypes.Type(value = CheckElementVisibleCommand.class, name = "checkElementVisible")
})
public abstract class Command {
    protected String action;
    protected String params;
    protected String description;

    public Command() {

    }

    public Command(String action, String params, String description) {
        this.action = action;
        this.params = params;
        this.description = description;
    }

    @JsonProperty("action")
    public String getAction() {
        return action;
    }

    @JsonProperty("action")
    public void setAction(String action) {
        this.action = action;
    }

    @JsonProperty("params")
    public String getParams() {
        return params;
    }

    @JsonProperty("params")
    public void setParams(String params) {
        this.params = params;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public abstract boolean run(WebDriver driver);
}
