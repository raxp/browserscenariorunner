package ru.raxp.browserscenariorunner.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class ClickCommand extends Command {
    public ClickCommand() {
        super();
    }

    public ClickCommand(String action, String params, String description) {
        super(action, params, description);
    }

    public boolean run(WebDriver driver) {
        try {
            System.out.printf("Clicking on %s\n", params);
            driver.findElement(By.xpath(params)).click();
            return true;
        } catch (NoSuchElementException e) {
            System.out.printf("No such element on page: %s\n", params);
            return false;
        }
    }
}
