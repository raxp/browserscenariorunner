package ru.raxp.browserscenariorunner.actions;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileOutputStream;

public class ScreenshotCommand extends Command {
    public static final String DEFAULT_SCREENSHOT_PATH = "screenshots";

    public static String screenshotPath = DEFAULT_SCREENSHOT_PATH;
    public static int screenshotsCount = 1;

    public ScreenshotCommand() {
        super();
    }

    public ScreenshotCommand(String action, String params, String description) {
        super(action, params, description);
    }

    public boolean run(WebDriver driver) {
        System.out.println("Saving screenshot");
        TakesScreenshot screenshot = (TakesScreenshot) driver;
        byte[] image = screenshot.getScreenshotAs(OutputType.BYTES);

        try {
            File dir = new File(screenshotPath);
            if (!dir.exists())
                dir.mkdirs();

            FileOutputStream dstFileStream =
                    new FileOutputStream(String.format("%s/screenshot%d.png", screenshotPath, screenshotsCount));
            dstFileStream.write(image);
            dstFileStream.close();
            screenshotsCount++;
            return true;
        } catch (Exception e) {
            System.out.println("Error saving screenshot");
            return false;
        }

    }
}
