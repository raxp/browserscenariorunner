package ru.raxp.browserscenariorunner;

public class Main {
    public static void main(String[] args) {
        //Usage: java -jar browserscenariorunner.jar [SCENARIO PATH] [SCREENSHOT DIR PATH] [CHROME_WEB_DRIVER_PATH]
        //Default scenario: ./test.json
        //Default screenshot path: ./screenshots
        //Default WebDriver ./path: chromedriver.exe
        String scenarioFilePath = args.length > 0 ? args[0] : null;
        String screenshotDirPath = args.length > 1 ? args[1] : null;
        String chromeDriverPath = args.length > 2 ? args[2] : null;

        BrowserScenarioRunner runner = new BrowserScenarioRunner(chromeDriverPath, screenshotDirPath);
        if (runner.populateCommandsFromFile(scenarioFilePath)) {
            runner.run();
        }
    }
}
